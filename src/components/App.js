import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Popup from "reactjs-popup";

import Menu from "./Menu";
import BurgerIcon from "./BurgerIcon";
import "../index.css";
import LandingPage from "./LandingPage";
import Android from "./Android";
import Web from "./Web";

function App() {
  return (
    <div className="App">
      <div>
        <div>
          <h3 className="logo">Victor Teka</h3>
        </div>
        <div>
          <Router>
            {" "}
            <Popup
              modal
              overlayStyle={{ background: "rgba(255,255,255,0.98" }}
              contentStyle={contentStyle}
              closeOnDocumentClick={false}
              trigger={(open) => <BurgerIcon open={open} />}
            >
              {(close) => <Menu close={close} />}
            </Popup>
            <Route exact path="/" component={LandingPage} />
            <Route path="/android" component={Android} />
            <Route path="/web" component={Web} />
          </Router>
        </div>
      </div>
    </div>
  );
}

const contentStyle = {
  background: "rgba(255,255,255,0)",
  width: "100%",
  border: "none",
};

export default App;
