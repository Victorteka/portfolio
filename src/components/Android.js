import React from "react";
import SplashScreen from "../assets/kulapoa/splashscreen.png";
import SignIn from "../assets/kulapoa/signin.png";
import HomeScreen from "../assets/kulapoa/landing.png";
import FavScreen from "../assets/kulapoa/fav.png";
import CartScreen from "../assets/kulapoa/cart.png";
import ProfileScreen from "../assets/kulapoa/profile.png";
import ProductScreen from "../assets/kulapoa/product.png";
import AboutScreen from "../assets/kulapoa/about.png";

import Popular from "../assets/movie/popular.png"
import Upcoming from "../assets/movie/upcoming.png"
import Details from "../assets/movie/details.png"
import Latest from "../assets/movie/latest.png"
import Trending from "../assets/movie/trend.png"
import Search from "../assets/movie/search.png"
import Tv from '../assets/movie/tv.png'




function Android() {
  return (
    <div style={{ backgroundColor: "#282c34" }}>
      <h1 style={andHeaderStyle}>Android Projects</h1>
      <div className="ui container" style={{ paddingBottom: "30px" }}>
        <div className="ui green segment">
        <div className="ui horizontal divider">
            <h2>kulapoa App</h2>
          </div>
          <div style={kpDescContainer}>
            <p style={kpDesc}>
              An Android app that enables users to order for fruits, vegetables
              and cereals online. It's an app I did for a client together with a
              team of three guys. Because of the NDA the app is not open source
              so I won't share the code.
            </p>
          </div>
          <div style={{ textAlign: "center", marginTop: "20px", marginBottom:"20px" }}>
            <a
              style={links}
              href="https://play.google.com/store/apps/details?id=com.app.kulapoa"
              target="_blank"
              rel="noopener noreferrer"
            >
              Visit playstore
            </a>
          </div>
          <div style={kpstyleContainer}>
            <img style={kpScreenShot} src={SplashScreen} alt="splashscreen" />
            <img style={kpScreenShot} src={SignIn} alt="SignIn" />
            <img style={kpScreenShot} src={HomeScreen} alt="HomeScreen" />
            <img style={kpScreenShot} src={FavScreen} alt="FavScreen" />
            <img style={kpScreenShot} src={CartScreen} alt="cart" />
            <img style={kpScreenShot} src={ProfileScreen} alt="ProfileScreen" />
            <img style={kpScreenShot} src={ProductScreen} alt="ProductScreen" />
            <img style={kpScreenShot} src={AboutScreen} alt="AboutScreen" />
          </div>
          
          <div className="ui horizontal divider">
            <h2>Movie App</h2>
          </div>
          <div style={kpDescContainer}>
            <p style={kpDesc}>
              An Android app that uses TMDB api to display trending, popular and latest movies.
            </p>
          </div>
          <div style={{ textAlign: "center", marginTop: "20px", marginBottom:"20px" }}>
            <a
              style={links}
              href="https://github.com/Victorteka/The_Movie_DB_Android_Client"
              target="_blank"
              rel="noopener noreferrer"
            >
              Visit Github
            </a>
          </div>
          <div style={kpstyleContainer}>
            <img style={kpScreenShot} src={Upcoming} alt="splashscreen" />
            <img style={kpScreenShot} src={Popular} alt="splashscreen" />
            <img style={kpScreenShot} src={Details} alt="SignIn" />
            <img style={kpScreenShot} src={Latest} alt="HomeScreen" />
            <img style={kpScreenShot} src={Trending} alt="HomeScreen" />
            <img style={kpScreenShot} src={Search} alt="FavScreen" />
            <img style={kpScreenShot} src={Tv} alt="cart" />
          </div>
        </div>
        
      </div>
    </div>
  );
}

const andHeaderStyle = {
  textAlign: "center",
  paddingTop: "120px",
  color: "#FFF",
  fontSize: "36px",
};

const kpstyleContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
};

const kpScreenShot = {
  width: "20%",
  paddingRight: "20px",
  paddingTop: "6px",
};

const kpDescContainer = {
  width: "75%",
  margin: "auto",
};

const kpDesc = {
  textAlign: "center",
  fontFamily: "Lato",
  fontSize: "16px",
};

const links = {
  padding: "8px 12px",
  border: "1px solid",
  marginRight: "16px",
  borderRadius: "20px",
  backgroundColor: "#4831d4",
  color: "#ccf381",
};

export default Android;
