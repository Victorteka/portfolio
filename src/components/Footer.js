import React from "react";

function Footer() {
  return (
    <div style={footerStyle}>
      <div style={footerContainer} className="ui container">
        <div>
          <h3>Get in touch</h3>
          <p>ambukateka@gmail.com</p>
          <p>Telegram</p>
        </div>
        <div>
          <p>My Resume</p>
          <p>My projects</p>
          <p>About me</p>
        </div>
      </div>
      <div style={dividerStyle} className="ui container"></div>
      <div style={socialMediaContainer} className="ui container">
        <p>&copy; Victor Teka 2020</p>
        <div>
          <a
            href="https://twitter.com/vicky_teka"
            target="_blank"
            rel="noopener noreferrer"
            style={{ color: "#ccf381" }}
          >
            {" "}
            <i style={socialMedia} className="twitter icon"></i>
          </a>
          <a
            href="https://github.com/Victorteka"
            target="_blank"
            rel="noopener noreferrer"
            style={{ color: "#ccf381" }}
          >
            {" "}
            <i style={socialMedia} className="github icon"></i>
          </a>
          <a
            href="https://www.linkedin.com/in/victor-teka/"
            target="_blank"
            rel="noopener noreferrer"
            style={{ color: "#ccf381" }}
          >
            {" "}
            <i style={socialMedia} className="linkedin icon"></i>
          </a>
          <a
            href="https://www.instagram.com/ambukateka/"
            target="_blank"
            rel="noopener noreferrer"
            style={{ color: "#ccf381" }}
          >
            {" "}
            <i style={socialMedia} className="instagram icon"></i>
          </a>
        </div>
      </div>
    </div>
  );
}

const footerStyle = {
  height: "100vh",
  backgroundColor: "#3d155f",
  paddingTop: "180px",
  color: "#ccf381",
};

const footerContainer = {
  display: "flex",
  justifyContent: "space-around",
};

const dividerStyle = {
  border: "1px solid",
  marginTop: "75px",
};

const socialMediaContainer = {
  display: "flex",
  justifyContent: "center",
  paddingTop: "120px",
};

const socialMedia = {
  paddingLeft: "50px",
};

export default Footer;
