import React from "react";
import NewsLanding from "../assets/newsApp/landing_news.png";
import Facebook from "../assets/facebook-clone/fb-clone.png"
import './Web.css'

function Web() {
  return (
    <div style={{ backgroundColor: "#4831d4", paddingBottom: "30px" }}>
      <h1 style={webHeaderStyle}>Web Projects</h1>
      <div className="ui container">
        <div className="ui blue segment" style={{ paddingBottom: "25px" }}>
          <div style={{ textAlign: "center" }}>
          <div className="ui horizontal divider">
            <h2>NYT News App</h2>
          </div>
          <div style={newsDescContainer}>
            <p style={newsDesc}>
              A website that displays timely news from the New Tork Times API. I
              did this as a practice project in order to get acquainted with
              consuming API with React js. Main technology was React and Redux
            </p>
          </div>
          <div style={{ textAlign: "center", marginTop: "20px", marginBottom:"20px" }}>
            <a
              style={links}
              href="https://news-app-nyt.netlify.app/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Visit website
            </a>
            <a
              style={links}
              href="https://github.com/Victorteka/React_News_App"
              target="_blank"
              rel="noopener noreferrer"
            >
              Visit Github
            </a>
          </div>
            <img
              style={newsLandingStyle}
              src={NewsLanding}
              alt="news landing page"
            />
          </div>
          
      <div style={{ textAlign: "center" }}>
      <div className="ui horizontal divider">
            <h2>Facebook clone</h2>
          </div>
          <div style={newsDescContainer}>
            <p style={newsDesc}>
              A clone of facebook web app. Done using Reactjs, firebase and material ui.
            </p>
          </div>
          <div style={{ textAlign: "center", marginTop: "20px", marginBottom:"20px" }}>
            <a
              style={links}
              href="https://fb-clone-aa9b9.web.app/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Visit website
            </a>
            <a
              style={links}
              href="https://github.com/Victorteka/React_Facebook_Clone"
              target="_blank"
              rel="noopener noreferrer"
            >
              Visit Github
            </a>
          </div>
            <img
              style={newsLandingStyle}
              src={Facebook}
              alt="news landing page"
            />
          </div>
          
        </div>
      </div>
    </div>
  );
}

const webHeaderStyle = {
  textAlign: "center",
  paddingTop: "120px",
  color: "#ccf381",
  fontWeight: "bold",
  fontSize: "36px",
};

const newsLandingStyle = {
  width: "75%",
  textAlign: "center",
};

const newsDescContainer = {
  width: "75%",
  margin: "auto",
};

const newsDesc = {
  textAlign: "center",
  fontFamily: "Lato",
  fontSize: "16px",
};

const links = {
  padding: "8px 12px",
  border: "1px solid",
  marginRight: "16px",
  borderRadius: "20px",
  backgroundColor: "#4831d4",
  color: "#ccf381",
};

export default Web;
